<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, $dui)
 */
class Record extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dui', 'nombre'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
