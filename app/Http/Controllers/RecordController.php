<?php


namespace App\Http\Controllers;

use App\Models\Record;
use Illuminate\Http\Request;

class RecordController extends Controller
{
    public function consultRecord(Request $request)
    {
        $filt = $request->json()->get('filt');
        $dui = $filt[0]['value'];

        try {
            $record = Record::where('dui', $dui)->firstOrFail();

            if ($record) {
                $full_name = explode(' ', $record->nombre);
                $nom1 = $full_name[0];
                $nom2 = $full_name[1];
                $ape1 = $full_name[2];
                $ape2 = $full_name[3];
                $apdoCsda = $full_name[4];

                $data = new \stdClass();
                $data->nom1 = $nom1;
                $data->nom2 = $nom2;
                $data->ape1 = $ape1;
                $data->ape2 = $ape2;
                $data->apdoCsda = $apdoCsda;

                return response()->json([
                    'resp' => 'true',
                    'data' => $data
                ]);
            } else {
                return response()->json([
                    'status' => 'failure',
                    'data' => 'Sin datos'
                ], 404);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'failure',
                'data' => $e->getMessage()
            ], 404);
        }
    }
}
